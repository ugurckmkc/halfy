//
//  PostTableViewCell.swift
//  Halfy
import UIKit
import Firebase

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postImagaView1: UIImageView!
    @IBOutlet weak var like1Button: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var labelChoosed: UILabel!
    @IBOutlet weak var labelChoosed1: UILabel!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var likeCounter1: UILabel!
    
    var postID : String!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        self.titleLabel.alpha = 0
        self.postImageView.alpha = 0
        self.postImageView.alpha = 0
        self.labelChoosed.alpha = 0
        self.labelChoosed1.alpha = 0
        self.likeCounter.alpha = 0
        self.likeCounter1.alpha = 0
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func likeTapped(_ sender: Any) {
        print("pressed like button 1")
        self.likeButton.isEnabled = false
        let ref = Database.database().reference()
        //let keyToPost = ref.child("posts").childByAutoId().key
        
        ref.child("posts").child(self.postID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let post = snapshot.value as? [String : AnyObject] {
                let updateLikes: [String : Any] = ["peopleWhoLike/\(self.postID as! String)" : Auth.auth().currentUser!.uid]
                ref.child("posts").child(self.postID).updateChildValues(updateLikes, withCompletionBlock: { (error, reff) in
                    
                    if error == nil {
                        ref.child("posts").child(self.postID).observeSingleEvent(of: .value, with: { (snap) in
                            if let properties = snap.value as? [String : AnyObject] {
                                if let likes = properties["peopleWhoLike"] as? [String : AnyObject] {
                                    let count = likes.count
                                    self.likeCounter.text = "\(count)"
                                    
                                    let update = ["imageLike" : count]
                                    ref.child("posts").child(self.postID).updateChildValues(update)
                                    
                                   
                                    self.like1Button.isEnabled = false
                                    
                                }
                            }
                        })
                    }
                })
            }
            
            
        })
        
        ref.removeAllObservers()
        
    }
    @IBAction func like1Tapped(_ sender: Any) {
        print("pressed like button 2")
        self.like1Button.isEnabled = false
        let ref = Database.database().reference()
        //let keyToPost = ref.child("posts").childByAutoId().key
        
        ref.child("posts").child(self.postID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let post = snapshot.value as? [String : AnyObject] {
                let updateLikes: [String : Any] = ["peopleWhoLike1/\(self.postID as! String)" : Auth.auth().currentUser!.uid]
                ref.child("posts").child(self.postID).updateChildValues(updateLikes, withCompletionBlock: { (error, reff) in
                    
                    if error == nil {
                        ref.child("posts").child(self.postID).observeSingleEvent(of: .value, with: { (snap) in
                            if let properties = snap.value as? [String : AnyObject] {
                                if let likes = properties["peopleWhoLike1"] as? [String : AnyObject] {
                                    let count = likes.count
                                    self.likeCounter1.text = "\(count)"
                                    
                                    let update = ["image1Like" : count]
                                    ref.child("posts").child(self.postID).updateChildValues(update)
                                    
                                    
                                    self.likeButton.isEnabled = false
                                    
                                }
                            }
                        })
                    }
                })
            }
            
            
        })
        
        ref.removeAllObservers()
        
    }
    
    
}

