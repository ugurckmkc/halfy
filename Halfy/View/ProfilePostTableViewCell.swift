//
//  ProfilePostTableViewCell.swift
//  Halfy
//
//  Created by MacOS*VIrtual on 4.01.2018.

import UIKit
import Firebase

class ProfilePostTableViewCell: UITableViewCell {

    
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var postRate: UILabel!
    @IBOutlet weak var postRate1: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postImage1: UIImageView!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var percLabel: UILabel!
    @IBOutlet weak var rateLabel1: UILabel!
    @IBOutlet weak var percLabel1: UILabel!
    
    var imageLike : Int!
    var image1Like : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.Title.alpha = 0
        self.postRate.alpha = 0
        self.postRate1.alpha = 0
        self.postImage.alpha = 0
        self.postImage1.alpha = 0
        self.rateLabel.alpha = 0
        self.rateLabel1.alpha = 0
        self.percLabel.alpha = 0
        self.percLabel1.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
