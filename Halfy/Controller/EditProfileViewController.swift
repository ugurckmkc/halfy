//
//  EditProfileViewController.swift
//  Halfy
//
//  Created by MacOS*VIrtual on 12.11.2017.

import UIKit
import Firebase

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameText: UITextField!
    
    @IBOutlet weak var displayNameText: UITextField!
    @IBOutlet weak var bioText: UITextField!
    
    
    var databaseReference:DatabaseReference!
    var storageReference:StorageReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        profileImageView.clipsToBounds = true
        storageReference = Storage.storage().reference()
        databaseReference = Database.database().reference()
        
        
        loadProfileData()
    }
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func changePhotoButton(_ sender: Any) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(picker, animated: true, completion: nil)
        
    }
    
    @IBAction func saveAllProfileChange(_ sender: Any) {
        
        updateProfile()
        self.dismiss(animated: true, completion: nil) 
    }
    
    func loadProfileData(){

        databaseReference = Database.database().reference()
        
        if let userID = Auth.auth().currentUser?.uid{
            
            databaseReference.child("profile").child(userID).observe(.value, with:{(snapshot) in
                
                let values = snapshot.value as? NSDictionary
                
                if let profileImageURL = values?["photo"] as? String{
                    
                    self.profileImageView.sd_setImage(with: URL(string: profileImageURL))
                    
                }
                self.usernameText.clearsOnBeginEditing = true
                self.usernameText.text = values?["username"] as? String
                
                self.displayNameText.clearsOnBeginEditing = true
                self.displayNameText.text = values?["display"] as? String
                
                self.bioText.clearsOnBeginEditing = true
                self.bioText.text = values?["bio"] as? String
            })
        }
    }
    
    func imagePickerController(_ picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]){
        
        var selectedImageFromPicker:UIImage?
        
        if let editedImage = info["UIImagePickerCOntrollerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker{
            profileImageView.image = selectedImage
            profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
            profileImageView.clipsToBounds = true	
        }
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("picker dic cancel")
        dismiss(animated: true, completion: nil)
	    }
    
    func updateProfile(){
        if let userID = Auth.auth().currentUser?.uid{
            
            let storageItem = storageReference.child("profile_images").child(userID)
            
            if let uploadData = UIImagePNGRepresentation(self.profileImageView.image!)
            {
                storageItem .putData(uploadData, metadata: nil, completion:{(metadata, error) in
                    if error != nil{
                        print(error!)
                        return
                    }
                    storageItem.downloadURL(completion: {(url, error) in
                        if error != nil{
                            print("error with url")
                            return
                        }
                        if let urlText = url?.absoluteString{
                            guard let newUserName = self.usernameText.text else {return}
                            guard let newDisplayName = self.displayNameText.text else{return}
                            guard let newBioText = self.bioText.text else{return}
                            
                            let newValuesForProfile = [
                                "photo": urlText, "username":newUserName,
                                "display": newDisplayName, "bio": newBioText]
                            self.databaseReference.child("profile").child(userID).updateChildValues(newValuesForProfile, withCompletionBlock:{(error, ref) in if error != nil{
                                print("error loading ", error!)
                                return
                            }else{print("profile updated")}
                            })
                        }
                    })
                })
            }
        }
    }
}
