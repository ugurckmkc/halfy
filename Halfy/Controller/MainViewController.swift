//
//  MainViewController.swift
//  Halfy
//


import UIKit
import Firebase
import FirebaseStorage

class MainViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var postsTableView: UITableView!
    var posts = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.postsTableView.delegate = self
        self.postsTableView.dataSource = self
        
        self.navigationController?.navigationBar.alpha = 0
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 193/255, blue: 148/255, alpha: 1.0)
       /* self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Billabong", size: 35)!, NSAttributedStringKey.foregroundColor: UIColor.white]*/
        
        loadData()
    }
    
    func loadData() {
        Database.database().reference().child("posts").observeSingleEvent(of: .value, with: { (snapshot) in
            if let postsDictionary = snapshot.value as? [String: AnyObject] {
                for post in postsDictionary {
                    self.posts.add(post.value)
                }
                self.postsTableView.reloadData()
            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PostTableViewCell
        // Configure the cell...
        print(posts[indexPath.row])
        let post = self.posts[indexPath.row] as! [String: AnyObject]
        
        cell.postID = post["postID"] as? String
        cell.titleLabel.text = post["title"] as? String
        if let likes = post["peopleWhoLike"] as? [String : AnyObject] {
            let count = likes.count
            cell.likeCounter.text = "\(count)"
        }
        if let likes1 = post["peopleWhoLike1"] as? [String : AnyObject] {
                let count = likes1.count
                cell.likeCounter1.text = "\(count)"
        }
        
        //enable or disable like button
        let uid = Auth.auth().currentUser?.uid
        
        if let userLikedDict = post["peopleWhoLike"] as? [String : String]{
            for userLiked in userLikedDict{
                if userLiked.value == uid{
                    cell.like1Button.isEnabled = false
                    cell.likeButton.isEnabled = false
                }
            }
        }
        if let userLikedDict1 = post["peopleWhoLike1"] as? [String : String]{
            for userLiked1 in userLikedDict1{
                if userLiked1.value == uid{
                    cell.like1Button.isEnabled = false
                    cell.likeButton.isEnabled = false
                }
            }
        }
        
        
        //filling image cells
        if let imageName = post["image"] as? String {
            let imageRef = Storage.storage().reference().child("images/\(imageName)")
            imageRef.getData(maxSize: 25 * 1024 * 1024, completion: { (data, error) -> Void in
                if error == nil {
                    if let imageName1 = post["image1"] as? String{
                        let imageRef1 = Storage.storage().reference().child("images/\(imageName1)")
                        imageRef1.getData(maxSize: 25*1024*1024, completion: { (data1, error) ->Void in
                            if error == nil{
                                // successful
                                let image = UIImage(data: data!)
                                cell.postImageView.image = image
                                let image1 = UIImage(data:data1!)
                                cell.postImagaView1.image=image1
                                cell.titleLabel.alpha = 0
                                cell.postImageView.alpha = 0
                                cell.postImagaView1.alpha = 0
                                cell.likeCounter.alpha = 0
                                cell.likeCounter1.alpha = 0
                                cell.labelChoosed.alpha = 0
                                cell.labelChoosed1.alpha = 0
                                
                                UIView.animate(withDuration: 0.4, animations: {
                                    cell.titleLabel.alpha = 1
                                    cell.postImageView.alpha = 1
                                    cell.postImagaView1.alpha = 1
                                    cell.likeCounter.alpha = 1
                                    cell.likeCounter1.alpha = 1
                                    cell.labelChoosed.alpha = 1
                                    cell.labelChoosed1.alpha = 1
                                    
                                })
                            }else {
                                print("Error downloading image: \(String(describing: error?.localizedDescription))")
                            }
                        })
                    }
                    
                } else {
                    // error
                    print("Error downloading image: \(String(describing: error?.localizedDescription))")
                }
            })
        }
        
        return cell
    }
    @IBAction func logOutTapped(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
            self.present(vc!, animated: true, completion: nil)
        } catch {
            
            print("Error signing out user.")
        }
    }
    
    
}


