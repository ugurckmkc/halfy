//
//  PostViewController.swift
//  Halfy
//

import UIKit
import Firebase
import FirebaseStorage

class PostViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var titleTextField: UITextField!
    
    
    var imageFileName = ""
    var imageFileName1 = ""
    
    var pickerController: Int?
    var dbReference:DatabaseReference?
    var handle:DatabaseHandle?
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var previewImageView1: UIImageView!
    @IBOutlet weak var selectImageButton1: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerController = 0
        
        self.navigationController?.navigationBar.alpha = 0
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255, green: 193/255, blue: 148/255, alpha: 1.0)
    }

    @IBAction func postTapped(_ sender: Any) {
        
        if (self.imageFileName != "" && self.imageFileName1 !=  ""){
        
            if let uid = Auth.auth().currentUser?.uid{
                dbReference = Database.database().reference()
                
                handle = dbReference?.child("profile").child(uid).child("username").observe(.value, with: { (snapshot) in
                    if let value = snapshot.value as? String{
                        
                        
                        let key = self.dbReference?.child("posts").childByAutoId().key
                        let username = snapshot.value as? String
                        let title = self.titleTextField.text
                        let postObject: Dictionary<String,Any> = [
                                "uid" : uid,
                                "title" : title,
                                "username" : username,
                                "image" : self.imageFileName,
                                "imageLike": 0,
                                "image1" : self.imageFileName1,
                                "image1Like": 0,
                                "postID" : key
                                ]
                        self.dbReference?.child("posts").child(key!).updateChildValues(postObject)
                       
                        
                        let alert = UIAlertController(title: "Success", message: "Your post has been sent!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            // code will run when "OK" button is tapped
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                            self.present(vc!, animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        print("Posted to Firebase.")
                        
                    }
                })
            }
        }
        
    }
    
    @IBAction func selectImageTapped(_ sender: Any) {
        
        pickerController = 0
        let picker = UIImagePickerController()
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    @IBAction func selectImageTapped1(_ sender: Any) {
        
        pickerController = 1
        let picker = UIImagePickerController()
        picker.delegate = self
        self.present(picker, animated: true,completion: nil)
    }
    
    func uploadImage(image: UIImage) {
        let randomName = randomStringWithLength(length: 10)
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        let uploadRef = Storage.storage().reference().child("images/\(randomName).jpg")
        
        _ = uploadRef.putData(imageData!, metadata: nil) { metadata, error in
            if error == nil {
                // success
                print("Successfully uploaded image.")
                if(self.pickerController == 0){
                    self.imageFileName = "\(randomName as String).jpg"}
                if(self.pickerController == 1){
                    self.imageFileName1 = "\(randomName as String).jpg"}
                
            } else {
                // error
                print("Error uploading image: \(String(describing: error?.localizedDescription))")
            }
        }
    }
    
    func randomStringWithLength(length: Int) -> NSString {
        let characters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString: NSMutableString = NSMutableString(capacity: length)
        
        for _ in 0..<length {
            let len = UInt32(characters.length)
            let rand = arc4random_uniform(len)
            randomString.appendFormat("%C", characters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if(pickerController==0)
            {
                self.previewImageView.image = pickedImage
                self.selectImageButton.isEnabled = false
                self.selectImageButton.isHidden = true
                uploadImage(image: pickedImage)
                picker.dismiss(animated: true, completion: nil)
            }
            if(pickerController==1)
            {
                self.previewImageView1.image = pickedImage
                self.selectImageButton1.isEnabled = false
                self.selectImageButton1.isHidden = true
                uploadImage(image: pickedImage)
                picker.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
