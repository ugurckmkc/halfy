//
//  LogInViewController.swift
//  Halfy
//
//  Created by Hasan Uğur ÇAKMAKÇIOĞLU on 22/10/2017.

import UIKit
import FirebaseAuth


class LogInViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
      /* if(Auth.auth().currentUser != nil){
            goHome()
        }*/
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //login variables
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var actionLoginBtn: UIButton!
    
    @IBAction func actionLogin(_ sender: UIButton) {
        
        self.login()
    }
    func login(){
        if self.emailText.text == "" || self.passwordText.text == "" {
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            self.showAlert(title: "Error", message: "Please enter an email and password.")
        } else {
            Auth.auth().signIn(withEmail: self.emailText.text!, password: self.passwordText.text!) { (user, error) in
                if error == nil {
                    self.goHome()
                } else {
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    self.showAlert(title: "ERROR", message: "ERROR")
                }
            }
        }
    }
    func goHome(){
        //Go to the HomeViewController if the login is sucessful
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
        self.present(vc!, animated: true, completion: nil)
    }
    
}
