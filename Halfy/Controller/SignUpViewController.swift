//
//  SignUpViewController.swift
//  Halfy
//
//  Created by MacOS*VIrtual on 23.10.2017.

import UIKit
import FirebaseAuth
import FirebaseDatabase
class SignUpViewController: UIViewController {
    
    var databaseReference: DatabaseReference!
    //sign up variables
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var passwordAgainText: UITextField!
    @IBOutlet weak var actionSIgnUpBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        
        //access db reference
        databaseReference = Database.database().reference()
    }
    

    @IBAction func actionSignup(_ sender: UIButton) {
        guard let email = emailText.text , let password = passwordText.text , let passwordAgain = passwordAgainText.text else{return}
        signUp(email:email, password:password, passwordAgain: passwordAgain)
    }
    
    func createUser(_ user: User){ 
        let delimiter = "@"
        let email = user.email
        let uName = email?.components(separatedBy: delimiter)
        let newUser = [ "username":uName?[0],
                        "email":user.email,
                        "photo":"https://firebasestorage.googleapis.com/v0/b/halfy-2aa7f.appspot.com/o/empty-profile.png?alt=media&token=ae94225c-6868-4a5f-b1f6-49d3c501be8a"]
        
        self.databaseReference.child("profile").child(user.uid).updateChildValues(newUser as Any as! [AnyHashable : Any]) {(error,ref) in
            if error != nil{
                	print(error!)
            
            }
            print("profile successfully created")
        }
        
    }
    
    func signUp(email:String, password:String, passwordAgain:String){
        if (email.isEmpty) ||
            (password.isEmpty) ||
            (passwordAgain.isEmpty) {
            self.showAlert(title: "HATA", message: "Lütfen boş alan bırakmayınız")
        }
        if (password != passwordAgain) {
            self.showAlert(title: "HATA", message: "Şifreler aynı değil")
        }
        if ((password.count) < 6 && (passwordAgain.count) < 6){
            self.showAlert(title: "HATA", message: "Şifreniz en az 6 krakterden oluşmalıdır.")
        }
        else{
            // Kayıt burada yapılacak
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if let error = error{
                    self.showAlert(title: "HATA", message: error.localizedDescription)
                }else {
                    self.createUser(user!)
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LogInViewController
                    self.present(loginVC, animated: true, completion: nil)
                    self.showAlert(title: "Tebrikler", message: "Kullanıcı kaydınız tamamlanmıştır.")
                    
                }
            }
        }
    }
}
extension UIViewController{
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
