//
//  ProfileViewController.swift
//  Halfy
//
//  Created by MacOS*VIrtual on 9.11.2017.

import UIKit
import Firebase
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
class ProfileViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
   
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var bioText: UILabel!
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var displaynameText: UILabel!
    
    @IBOutlet weak var table: UITableView!
    var posts = NSMutableArray()
    
    var databaseReference: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        self.profilePicture.layer.cornerRadius = profilePicture.frame.size.width/2
        self.profilePicture.clipsToBounds = true
        
        self.table.delegate = self
        self.table.dataSource = self
        
        
        databaseReference = Database.database().reference()
        
        if let userID = Auth.auth().currentUser?.uid{
            databaseReference.child("profile").child(userID).observeSingleEvent(of:
                .value, with:{(snapshot) in
                    let dictionary = snapshot.value as? NSDictionary
                    let userName = dictionary?["username"] as? String ?? "username"
                    let displayName = dictionary?["display"] as? String ?? "display"
                    let bioText = dictionary?["bio"] as? String ?? "bio"
                 
                    if let profileImageURL = dictionary?["photo"] as? String{
                        let url = URL(string: profileImageURL)
                        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                            
                            if error != nil{
                                  print(error!)
                                return
                            }
                            DispatchQueue.main.async {
                                self.profilePicture.image = UIImage(data: data!)
                                
                            }
                        }).resume()
                    }
                    self.usernameText.text=userName
                    self.displaynameText.text=displayName
                    self.bioText.text=bioText
            }){(error) in
                print(error.localizedDescription)
                return
            }
        }
        
        loadData()
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
            self.present(vc!, animated: true, completion: nil)
        } catch {
            
            print("Error signing out user.")
        }

    }
    func loadData(){
        databaseReference.child("posts").observeSingleEvent(of: .value) { (snapshot) in
            if let postsDictionary = snapshot.value! as? [String : AnyObject]{
                for post in postsDictionary{
                   print(post)
                    if post.value["uid"] as? String == Auth.auth().currentUser?.uid{
                        print(post.value["uid"])
                        print(post.value)
                        self.posts.add(post.value)
                    }
                    print(self.posts)
                }
                self.table.reloadData()
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfilePostTableViewCell
        if cell == nil {
            cell = ProfilePostTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ProfileCell")
        }
        let post = self.posts[indexPath.row] as! [String : AnyObject]
        
        //fill content and title
        cell.Title.text = post["title"] as? String
        
        //calculate and fill post rate
        let imageLike=(post["imageLike"] as! Int)
        let image1Like=(post["image1Like"] as! Int)
        cell.postRate.text = String((imageLike*100)/(imageLike+image1Like))
        cell.postRate1.text = String((image1Like*100)/(image1Like+imageLike))
        
        
        print(post)
        //filling image cells
        if let imageName = post["image"] as? String{
            let imageRef = Storage.storage().reference().child("images/\(imageName)")
            imageRef.getData(maxSize: 25*1024*1024, completion: { (data, error) -> Void in
                if error == nil{
                    if let imageName1 = post["image1"] as? String{
                        let imageRef1 = Storage.storage().reference().child("images/\(imageName1)")
                        imageRef1.getData(maxSize: 25*1024*1024, completion: { (data1, error) -> Void in
                            if error == nil{
                                // successfull
                                let image = UIImage(data: data!)
                                cell.postImage.image = image
                                let image1 = UIImage(data: data1!)
                                cell.postImage1.image = image1
                                
                                //animations step 1
                                cell.Title.alpha = 0
                                cell.postImage.alpha = 0
                                cell.postImage1.alpha = 0
                                cell.rateLabel.alpha = 0
                                cell.rateLabel1.alpha = 0
                                cell.percLabel.alpha = 0
                                cell.percLabel1.alpha = 0
                                cell.postRate.alpha = 0
                                cell.postRate1.alpha = 0
                                
                                //animations step 2
                                UIView.animate(withDuration: 0.4, animations:{
                                    cell.Title.alpha = 1
                                    cell.postImage.alpha = 1
                                    cell.postImage1.alpha = 1
                                    cell.rateLabel.alpha = 1
                                    cell.rateLabel1.alpha = 1
                                    cell.percLabel.alpha = 1
                                    cell.percLabel1.alpha = 1
                                    cell.postRate.alpha = 1
                                    cell.postRate1.alpha = 1
                                })
                            }else {
                                print("Error downloading image: \(String(describing: error?.localizedDescription))")
                            }
                        
                        })
                    }
                }else{
                    //error
                    print("Error downloading image: \(String(describing: error?.localizedDescription))")
                }
            })
            
        }
        return cell
    }
    
}
